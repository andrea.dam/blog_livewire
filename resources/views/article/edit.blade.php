<x-layout>

    <x-header>
        Modifica l'articolo scelto
    </x-header>

    <main class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                @livewire('article-edit-form', ['articleId' => $article->id])
            </div>
        </div>
    </main>

</x-layout>