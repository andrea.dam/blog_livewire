<x-layout>
    
    <x-header>
        Tutti gli articoli
    </x-header>
    
    <main class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                {{-- @dd($articles) --}}
                @livewire('article-index-table')
            </div>
        </div>
    </main>
    
</x-layout>