<x-layout>

    <x-header>
        Inserisci il tuo articolo
    </x-header>

    <main class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                @livewire('article-create-form')
            </div>
        </div>
    </main>

</x-layout>