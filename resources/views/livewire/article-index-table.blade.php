<div>
    @if (session('articleDeleted'))
        <div class="alert alert-danger">
            {{session('articleDeleted')}}
        </div>
    @endif
    <table class="table table-striped border">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col">Subtitle</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($articles as $article)
            <tr>
                <td scope="row">{{$article->id}}</td>
                <td>{{$article->title}}</td>
                <td>{{$article->subtitle}}</td>
                <td class="col d-flex justify-content-around">
                    <a class="text-warning" href="{{route('article.edit', compact('article'))}}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>
                    <a class="text-danger" wire:click="destroy({{$article->id}})" role="button" href="#">
                        <i class="fa-solid fa-trash-can"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
