<div class="my-5">
    <form class="" action="" method="" wire:submit.prevent="store">
        <div>
            @if (session()->has('articleCreated'))
            <div class="alert alert-success">
                {{session('articleCreated')}}
            </div>
            @endif
        </div>
        
        @csrf
        <div class="mb-3">
            <label for="title" class="form-label">Titolo</label>
            <input type="text" wire:model.lazy="title" class="form-control @error('title') is-invalid @enderror" id="title">
            @error('title')
                <span class="fst-italic text-danger small">{{$message}}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="subtitle" class="form-label">Sottotitolo</label>
            <input type="text" wire:model.lazy="subtitle" class="form-control @error('subtitle') is-invalid @enderror" id="subtitle">
            @error('subtitle')
                <span class="fst-italic text-danger small">{{$message}}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="body" class="form-label">Testo</label>
            <textarea id="body" wire:model.lazy="body" type="text" cols="30" rows="10" class="form-control @error('body') is-invalid @enderror"></textarea>
            @error('body')
                <span class="fst-italic text-danger small">{{$message}}</span>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-success">Inserisci articolo</button>
        <a class="btn btn-secondary" href="{{route('homepage')}}" role="button">Torna alla Home</a>
    </form>
</div>
