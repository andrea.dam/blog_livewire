<x-layout>

    <x-header>AD Livewire</x-header>

    <main class="container p-5">
        <div class="row">
            <div class="col-6 text-end">
                @foreach ($articles as $article)
                    <h2>L'ultimo articolo</h2>
                    <h3>{{$article->title}}</h3>
                    <h4>{{$article->subtitle}}</h4>
                    <p>{{$article->body}}</p>
                @endforeach
            </div>
            <div class="col-6">

            </div>
        </div>
    </main>

</x-layout>