<header class="container-fluid text-center bg-primary text-white p-5">
    <div class="row justify-content-center">
        <h1 class="col display-1">{{$slot}}</h1>
    </div>
</header>