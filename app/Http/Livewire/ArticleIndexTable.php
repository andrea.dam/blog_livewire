<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;

class ArticleIndexTable extends Component
{
    public function destroy($id){
        Article::find($id)->delete();
        session()->flash('articleDeleted', 'Hai correttamente eliminato l\'articolo');
    }

    public function render()
    {
        $articles = Article::all();
        return view('livewire.article-index-table', compact('articles'));
    }
}
