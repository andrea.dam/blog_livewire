<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;

class ArticleEditForm extends Component
{
    public $articleId;
    public $title;
    public $subtitle;
    public $body;

    public function update() {
        Article::find($this->articleId)->update([
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'body' => $this->body,
        ]);

        session()->flash('articleUpdated', 'Hai correttamente aggiornato l\'articolo');
    }

    public function mount() {
        $article = Article::find($this->articleId);

        $this->title = $article->title;
        $this->subtitle = $article->subtitle;
        $this->body = $article->body;
    }

    public function render()
    {
        return view('livewire.article-edit-form');
    }
}
